/* global axios */
const form = document.querySelector('#todo-form');
const textbox = document.querySelector('#todo-text');
const container = document.querySelector('#todo-list');

let list = [];
let editing = false;

const API_URL = 'http://localhost:3000/todo_items/';
const axiosInstance = axios.create({
  baseURL: API_URL,
  timeout: 1000,
  headers: { 'Content-Type': 'application/json' }
});

async function apiRequest({method = 'GET', url = API_URL, data = null} = {}) {
  let response = await axiosInstance.request({
    method, url, data
  }).catch(error => {
    console.error(error);
  });

  if(response.status !== 200) {
    console.error(`problem with apiRequest (method: ${method}, url: ${url}, data: ${data}`);
  }

  return response;
}

async function createTodoItem({content}) {
  return apiRequest({
    method: 'POST',
    data: { content }
  })
}

async function updateTodoItem({id, content}) {
  return apiRequest({
    url: API_URL + id,
    method: 'PUT',
    data: { content }
  })
}

async function destroyTodoItem({id}) {
  return apiRequest({
    url: API_URL + id,
    method: 'DELETE'
  })
}

function createButton({id, text, parent, actionFunc}) {
  let button = document.createElement('button');
  button.id = id;
  button.innerHTML = text;
  button.style.marginLeft = '10px';
  button.addEventListener('click', actionFunc);

  parent.appendChild(button);
  return button;
}

function handleFormSubmit({form, textbox, actionFunc}) {
  form.addEventListener('submit', (evt) => {
    evt.preventDefault();

    if (textbox.value.trim() === '') return;

    actionFunc();
  })
}

async function handleEdit({index, text_holder, editMode}) {
  let current = list[index];

  document.getElementById('edit_button' + index).innerHTML = editing ? 'edit' : 'save';
  document.getElementById('delete_button' + index).innerHTML = editing ? 'delete' : 'cancel';

  for(let i = 0; i < list.length; i++) {
    if(i !== index) {
      document.getElementById('edit_button' + i).style.display = editing ? 'initial' : 'none';
      document.getElementById('delete_button' + i).style.display = editing ? 'initial' : 'none';
    }
  }

  if(editMode && editing) {
    let content = document.getElementById('edit_box' + index).value;
    current.content = content;

    await updateTodoItem({ id: current.id, content });
  }

  text_holder.textContent = editing ? current.content : '';
  editing = !editing;
}

async function updateList() {
  container.innerHTML = '';
  let index = 0;

  for(const item of list) {
    let current = index;
    let list_item = document.createElement('li');
    list_item.setAttribute('id', 'list_item' + current);

    let text_holder = document.createElement('span');
    text_holder.textContent = item.content;
    list_item.appendChild(text_holder);

    // start editing or save edit
    createButton({id: 'edit_button' + current, text: 'edit', parent: list_item, actionFunc: async () => {
      await handleEdit({index: current, text_holder, editMode: true});

      if(editing) {
        let edit_form = document.createElement('form');
        edit_form.setAttribute('onsubmit', 'return false');
        text_holder.appendChild(edit_form);

        let edit_box = document.createElement('input');
        edit_box.setAttribute('id', 'edit_box' + current);
        edit_box.setAttribute('type', 'text');
        edit_box.setAttribute('maxlength', '100');
        edit_box.value = list[current].content;
        edit_form.appendChild(edit_box);
      }
    }});

    // delete item or cancel edit
    createButton({id: 'delete_button' + current, text: 'delete', parent: list_item, actionFunc: async () => {
      if(editing) {
        await handleEdit({index: current, text_holder, editMode: false});
      } else {
        let response = await destroyTodoItem({ id: item.id });

        if(response.status === 200) {
          list.splice(current, 1);
          await updateList();
        }
      }
    }});

    container.appendChild(list_item);
    index++;
  }
}

handleFormSubmit({form, textbox, actionFunc: async () => {
    textbox.disabled = true;

    let response = await createTodoItem({ content: textbox.value });

    if(response.status === 200) {
      list.push(response.data);
      await updateList();
      textbox.value = '';
    }

    textbox.disabled = false;
    textbox.focus();
    textbox.select();
}});

async function init() {
  const { data } = await apiRequest();
  list = data;

  for(const item of list) {
    console.log(item);
  }

  await updateList();
}

init().then(() => { console.log('ready') });
